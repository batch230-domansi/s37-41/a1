const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

//Creating a course
// Activity
// 1. Refractor the "course" route to implement user authentication for the admin when creating a course.

router.post("/create", (request, response) => {
	courseControllers.addCourse(request.body).then(resultFromControllers => response.send(resultFromControllers))
})

// Get all courses
router.get("/all", (request, response) => {
	courseControllers.getAllCourse().then(resultFromControllers => response.send(resultFromControllers))
})

// Get all ACTIVE courses
router.get("/active", (request, response) => {
	courseControllers.getActiveCourses().then(resultFromControllers => response.send(resultFromControllers))
})


router.get("/:courseId", (request, response) => {
	courseControllers.getCourse(request.params.courseId).then(resultFromControllers => response.send(resultFromControllers))
})

router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


// Activity S40

router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const courseId = request.params.courseId;
    const newData = {
		course: request.body, 
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.archiveCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


module.exports = router;