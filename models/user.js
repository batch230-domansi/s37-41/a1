const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
     { 
		firstName:{
	        type: String,
	        required: [true, "Name is required"]    
    	},
    	lastName:{
	       	type : String,
	       	required: [true,"Last name is required"]
    	},
    	email : {
       	    type: String, 
       	    required: [true,"Email is required"]
    	},
    	password: {
       	    type: String, 
       	    required: [true,"Password is required"]
       	},
       	isAdmin: {
       	    type: Boolean, 
       	    default: true
       	},
       	mobileNo: {
       	    type: String, 
       	    required: [true,"Mobile Number is required"]
       	},
       	enrollments:[
            {
                	courseId:{
                	type: String,
                	required:[true, "courseId is required"]
                },
                 	enrolledOn:{
                 	type: Date,
                	default: new Date()
                }
            }
   		]
	}
) 

module.exports= mongoose.model("User", UserSchema);



/*
,
                	status:{
                	type: String,
                	default: "Enrolled"

                }

*/