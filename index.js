
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js")


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/user", userRoutes);
app.use("/courses", courseRoutes);

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.yat49rm.mongodb.net/courseBooking?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Domansi-Mongo DB Atlas"));

app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});